package com.jiradev.transformation;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.plugin.servlet.DownloadableResource;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created by bhushan154 on 25/07/14.
 */
public class CustomDownloadableResource implements DownloadableResource {

    private static final Logger log = Logger.getLogger(CustomDownloadableResource.class);

    public String getContentType(){
        return "js";
    }

    public boolean isResourceModified(HttpServletRequest request, HttpServletResponse response){
        return false;
    }

    public void serveResource(HttpServletRequest request, HttpServletResponse response){
        String output = getOutputText();
        try {
            response.getWriter().write(output);
        }
        catch(Exception exc){
            log.error(exc.toString());
        }
    }

    public void streamResource(OutputStream outputStream){
        String output = getOutputText();
        try {
            outputStream.write(output.getBytes());
            outputStream.close();
        }
        catch(IOException exc){
            log.error(exc.toString());
        }
    }

    public String getOutputText(){
        String username = "Unknown User";
        ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
        if(confluenceUser != null)
            username = confluenceUser.getName();
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader br = null;
        try {
            String currentLine;
            br = new BufferedReader(new InputStreamReader(ClassLoaderUtils.getResourceAsStream("welcome-user.txt", this.getClass())));
            while ((currentLine = br.readLine()) != null) {
                if(currentLine.contains("<USERNAME>"))
                    currentLine = currentLine.replace("<USERNAME>", username);
                stringBuilder.append(currentLine + "\n");
            }

        } catch (IOException e) {
            log.error(e);
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
                log.error(ex);
            }
        }
        return stringBuilder.toString();
    }

}