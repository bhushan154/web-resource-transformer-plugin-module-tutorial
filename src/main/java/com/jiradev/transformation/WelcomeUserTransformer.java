package com.jiradev.transformation;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import org.apache.log4j.Logger;

/**
 * Created by bhushan154 on 25/07/14.
 */
public class WelcomeUserTransformer implements WebResourceTransformer {

    private static final Logger log = Logger.getLogger(WelcomeUserTransformer.class);

    @Override
    public DownloadableResource transform(org.dom4j.Element configElement, ResourceLocation location, String filePath, DownloadableResource nextResource){
        CustomDownloadableResource customDownloadableResource = new CustomDownloadableResource();
        return customDownloadableResource;
    }
}